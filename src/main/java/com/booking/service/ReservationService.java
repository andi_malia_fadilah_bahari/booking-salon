package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;


public class ReservationService {
    public static Reservation createReservation(List<Person> personList, List<Service> serviceList){
        Reservation reservation = null;
        Customer customer = null;
        do {
            PrintService.showAllCustomer(personList);
            String customerId = ValidationService.validateInput("Silahkan Masukkan Customer Id:", "Input Tidak Hanya Menerima Spasi", "^[^ %n]+$");
            
            customer = (Customer)getPersonById(personList, customerId);
            if (customer == null) {
                System.out.println("Customer yang dicari tidak tersedia");
            }
        } while (customer == null);

        Employee employee = null;
        do {
            PrintService.showAllEmployee(personList);
            String employeeId = ValidationService.validateInput("Silahkan Masukkan Employee Id:", "Input Tidak Hanya Menerima Spasi", "^[^ %n]+$");
            
            employee = (Employee)getPersonById(personList, employeeId);
            if (employee == null) {
                System.out.println("Employee yang dicari tidak tersedia");
            }
        } while (employee == null);

        List<Service> listSelectedServices = new ArrayList<>();
        List<Service> listNotSelectedService = serviceList;
        String loopAnswer = "Y";
        do {
            Service selectedServices = null;

            do {
                PrintService.showAllService(listNotSelectedService);
                String serviceId = ValidationService.validateInput("Silahkan Masukkan Service Id:", "Input Tidak Hanya Menerima Spasi", "^[^ %n]+$");
            
                selectedServices = getServiceById(listNotSelectedService, serviceId);
                Service selectedAllServices = getServiceById(serviceList, serviceId);
                if (selectedServices == null) {
                    if (selectedAllServices == null) {
                        System.out.println("Service yang dicari tidak tersedia");
                    }
                    else {
                        System.out.println("Service Sudah Dipilih");
                    }
                }
            } while (selectedServices == null);

            listNotSelectedService.remove(selectedServices);
            listSelectedServices.add(selectedServices);
            
            loopAnswer = ValidationService.validateInput("Tambahkan Service? (Y/T)", "Input Hanya Menerima Y/T", "^[tyTY]+$");
            if (loopAnswer.equalsIgnoreCase(loopAnswer)) {
                // 
            }
        } while (loopAnswer.equalsIgnoreCase("Y"));

        reservation = new Reservation(customer, employee, listSelectedServices, "In Process");

        System.out.println("Booking Berhasil!!");
        System.out.println("Total Biaya Booking "+reservation.getReservationPrice());

        return reservation;
    }

    public static Person getPersonById(List<Person> personList, String personId){
        return personList
        .stream()
        .filter(person -> person.getId().equalsIgnoreCase(personId))
        .findAny()
        .orElse(null);
    }

    public static Service getServiceById (List<Service> serviceList, String serviceId) {
        return serviceList
        .stream()
        .filter(serv -> serv.getServiceId().equalsIgnoreCase(serviceId))
        .findAny()
        .orElse(null);
    }

    public static List<Reservation> editReservationWorkstage(List<Reservation> listAllReservations){
        Reservation reservation = null;
        do {
            PrintService.showReservationWorkstage(listAllReservations);
            String reservationId = ValidationService.validateInput("Silahkan Masukkan Reservation Id:", "Input Tidak Hanya Menerima Spasi", "^[^ %n]+$");
            
            reservation = getReservationById(listAllReservations, reservationId);
            if (reservation == null) {
                System.out.println("Reservation Id yang dimasukan Salah Atau Tidak Tersedia");
            }
            else if (reservation.getWorkstage().equalsIgnoreCase("Finish")) {
                System.out.println("Reservation yang dicari sudah selesai");
                reservation = null;
            }
        } while (reservation == null);

        String[] avaiableWorkstage = {"Finish", "Canceled"};
        boolean isInAvaiableWorkstage = false;
        do {
            String changeWorkstage = ValidationService.validateInput("Ubah Workstage [Finish, Canceled]:", "Input Tidak Hanya Menerima Spasi", "^[a-zA-Z ]+$");
            
            for (String workstage : avaiableWorkstage) {
                if (changeWorkstage.equalsIgnoreCase(workstage)) {
                    reservation.setWorkstage(workstage);
                    reservation.calculateReservationPrice();
                    isInAvaiableWorkstage = true;
                }
            }
        } while (!isInAvaiableWorkstage);
        
        System.out.println("Reservasi dengan ID "+reservation.getReservationId()+ " Sudah " + reservation.getWorkstage());
        
        int reservationIndex = listAllReservations.indexOf(reservation);
        listAllReservations.set(reservationIndex, reservation);
        
        return listAllReservations;
    }

    public static Reservation getReservationById (List<Reservation> listAllReservations, String reservationId) {
        return listAllReservations
        .stream()
        .filter(reserv -> reserv.getReservationId().equalsIgnoreCase(reservationId))
        .findAny()
        .orElse(null);
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
    public static List<Reservation> getAllRecentReservations (List<Reservation> listAllReservations) {
        return listAllReservations
        .stream()
        .filter(reserv -> reserv.getWorkstage().equalsIgnoreCase("In Process"))
        .collect(Collectors.toList());
    }
    public static List<Reservation> getAllFinishedReservations (List<Reservation> listAllReservations) {
        return listAllReservations
        .stream()
        .filter(reserv -> reserv.getWorkstage().equalsIgnoreCase("Finish"))
        .collect(Collectors.toList());
    }
}
