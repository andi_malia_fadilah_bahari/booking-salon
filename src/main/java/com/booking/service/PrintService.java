package com.booking.service;

import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showReservation(List<Reservation> reservationList){
        String line = "+======+============+=================+====================================================+=================+=================+";
        String formatTable = "| %-4s | %-10s | %-15s | %-50s | %-15s | %-15s |%n";

        System.out.println(line);
        System.out.format(formatTable,"No", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println(line);
        reservationList
        .stream()
        .forEach(reserv -> {
            System.out.format(formatTable,
            reservationList.indexOf(reserv) + 1, 
            reserv.getReservationId(), 
            reserv.getCustomer().getName(), 
            printServices(reserv.getServices()), 
            reserv.getReservationPrice(),
            reserv.getWorkstage());
        });
        System.out.println(line);
    }

    public static void showAllCustomer(List<Person> personList){
        String line = "+======+============+==============+=================+============+=================+";
        String formatTable = "| %-4s | %-10s | %-12s | %-15s | %-10s | %-15s |%n";

        System.out.println(line);
        System.out.format(formatTable,"No", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println(line);

        List<Person> customer = personList
        .stream()
        .filter(person -> person instanceof Customer)
        .collect(Collectors.toList());

        customer
        .stream()
        .forEach(cust -> {
            System.out.format(formatTable, 
            customer.indexOf(cust) + 1,
            cust.getId(),
            cust.getName(),
            cust.getAddress(),
            ((Customer)cust).getMember().getMembershipName(),
            ((Customer)cust).getWallet());
        });
        System.out.println(line);
    }

    public static void showAllEmployee(List<Person> personList){
        String line = "+======+============+==============+=================+======================+";
        String formatTable = "| %-4s | %-10s | %-12s | %-15s | %-20s |%n";

        System.out.println(line);
        System.out.format(formatTable,"No", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println(line);

        List<Person> employee = personList
        .stream()
        .filter(person -> person instanceof Employee)
        .collect(Collectors.toList());

        employee
        .stream()
        .forEach(emp -> {
            System.out.format(formatTable, 
            employee.indexOf(emp) + 1,
            emp.getId(),
            emp.getName(),
            emp.getAddress(),
            ((Employee)emp).getExperience());
        });
        System.out.println(line);
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
        String line = "+======+============+=================+====================================================+=================+=================+";
        double totalBiaya = reservationList
        .stream()
        .mapToDouble(reserv -> reserv.getReservationPrice())
        .sum();
        
        showReservation(reservationList);
        System.out.format("| %-88s | %33s |%n", "Total Keuntungan", totalBiaya);
        System.out.println(line);
    }

    public static void showAllService (List<Service> serviceList) {
        String line = "+======+============+======================+=================+";
        String formatTable = "| %-4s | %-10s | %-20s | %-15s |%n";

        System.out.println(line);
        System.out.format(formatTable,"No", "ID", "Nama", "Harga");
        System.out.println(line);

        serviceList
        .stream()
        .forEach(service -> {
            System.out.format(formatTable,
            serviceList.indexOf(service) + 1,
            service.getServiceId(),
            service.getServiceName(),
            service.getPrice());
        });
        System.out.println(line);
    }

    public static void showReservationWorkstage (List<Reservation> reservationList) {
        String line = "+======+============+=================+====================================================+=================+";
        String formatTable = "| %-4s | %-10s | %-15s | %-50s | %-15s |%n";

        System.out.println(line);
        System.out.format(formatTable,"No", "ID", "Nama Customer", "Nama Service", "Total Biaya");
        System.out.println(line);

        reservationList
        .stream()
        .forEach(reservation -> {
            System.out.format(formatTable, 
            reservationList.indexOf(reservation) + 1,
            reservation.getReservationId(),
            reservation.getCustomer().getName(),
            printServices(reservation.getServices()),
            reservation.getReservationPrice());
        });
        System.out.println(line);
    }
}
