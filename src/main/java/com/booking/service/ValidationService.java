package com.booking.service;

import java.util.Scanner;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static String validateInput(String question, String errorMessage, String regex){
        Scanner input = new Scanner(System.in);
        boolean isLoop = true;
        String answer = null;
        do {
            System.out.print(question+" ");
            answer = input.nextLine();

            if (answer.matches(regex)) {
                isLoop = false;
            }
            else {
                System.out.println(errorMessage);
            }
        } while (isLoop);
        System.out.println();
        return answer;
    }

    public static void validateCustomerId(){

    }
}
