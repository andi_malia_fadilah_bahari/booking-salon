package com.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    private static int countId = 1;
    //   workStage (In Process, Finish, Canceled)

    public Reservation( Customer customer, Employee employee, List<Service> services,
            String workstage) {
        setReservationId("Rsv-00" + countId);
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.workstage = workstage;
        calculateReservationPrice();
        countId++;
    };

    public void calculateReservationPrice(){
        setReservationPrice(0);

        if (getWorkstage().equalsIgnoreCase("Finish")) {
            double discount = 0;
            if (getCustomer().getMember().getMembershipName().equalsIgnoreCase("Silver")) {
                discount = 0.05;
            }
            else if (getCustomer().getMember().getMembershipName().equalsIgnoreCase("Gold")) {
                discount = 0.1;
            }

            double curretPrice = getServices()
                                .stream()
                                .mapToDouble(services -> services.getPrice())
                                .sum();

            setReservationPrice( curretPrice - (curretPrice * discount) );
        }
    }
}
